<?php
require_once('RoundResult.php');

class RaceResult
{
    const OUTCOME_PENDING = 'pending';
    const OUTCOME_WIN = 'win';
    const OUTCOME_LOSE = 'lose';
    const OUTCOME_DRAW = 'draw';

    /**
     * @var array of StepResult
     */
    private $roundResults = [];

    private $winners = array();
    private $outcome = self::OUTCOME_PENDING;

    public function __construct(array $tracks_and_cars) {
        $this->winners = array();
        $step = 0;
        while (! $this->checkGoal($tracks_and_cars)) {
            $round_result = array();
            foreach ($tracks_and_cars as $car_idx => $track_and_car) {
                extract($track_and_car);
                $round_result[] = $this->recordRoundResult($step, !empty($this->roundResults) ? $this->roundResults[count($this->roundResults) - 1]->carsPosition[$car_idx] : 0, $track, $car);
            }
            $carsPosition = array_map(function($val) { return $val['end_track_pos']; }, $round_result);
            $this->roundResults[] = new RoundResult($step, $carsPosition);
            $step++;
        }
    }

    public function recordRoundResult($step, $pos, Track $track, Car $car) {
        $start_track_pos = $pos;
        $start_track_type = $track->elements[$pos]->type;
        $end_track_pos = $step > 0 ? $pos + $car->speed[$start_track_type] : 0;
        $end_track_type = $end_track_pos < $track->total ? $track->elements[$end_track_pos]->type : $start_track_type;

        if ($start_track_type != $end_track_type) {
            $traveled = array_slice($track->elements, $start_track_pos, $end_track_pos - $start_track_pos + 1);
            $traveled_types = array_map(function($val) { return $val->type; }, $traveled);
            $first_change_idx_in_traveled = array_search($end_track_type, $traveled_types);
            $first_change_idx = $start_track_pos + $first_change_idx_in_traveled;
            $end_track_pos = $first_change_idx;
            $end_track_type = $end_track_pos < $track->total ? $track->elements[$end_track_pos]->type : $start_track_type;
        }

        $round_result = compact('step', 'start_track_pos', 'start_track_type', 'end_track_pos', 'end_track_type', 'track', 'car');

        return $round_result;
    }

    public function checkGoal(array $tracks_and_cars) {
        if (empty($this->roundResults)) return false;
        $tracks_length = array();
        foreach ($tracks_and_cars as $car_idx => $track_and_car) {
            extract($track_and_car);
            $tracks_length[$car_idx] = $track->total;
        }
        foreach (end($this->roundResults)->carsPosition as $car_idx => $car_pos) {
            if ($car_pos >= $tracks_length[$car_idx]) $this->winners[] = $tracks_and_cars[$car_idx]['car'];
        }
        if (!empty($this->winners)) {
            switch ($winners_count = count($this->winners)) {
                case $winners_count <= 0: $this->outcome = self::OUTCOME_LOSE; break;
                case $winners_count == 1: $this->outcome = self::OUTCOME_WIN; break;
                case $winners_count > 1: $this->outcome = self::OUTCOME_DRAW; break;
                default: $this->outcome = self::OUTCOME_PENDING; break;
            }
            return true;
        }
        return false;
    }

    public function getRoundResults(): array
    {
        return $this->roundResults;
    }

    public function getRoundWinners(): array
    {
        return $this->winners;
    }

    public function getRoundOutcome(): string
    {
        return $this->outcome;
    }
}
