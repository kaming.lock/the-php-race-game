<?php

class Car
{
    public $id = 0;
    public $speed = array();
    public $total_speed = 0;

    private $min_speed = array();

    public function __construct() {
        $this->id = uniqid();
        $this->total_speed = 22;
        $remaining_speed_for_distribute = $this->total_speed;
        $refl = new ReflectionClass('Track_element');
        $types = array_filter($refl->getConstants(), function($key) { return substr($key, 0, 5) == 'TYPE_'; }, ARRAY_FILTER_USE_KEY);
        $i = 1;
        foreach ($types as $type) {
            $this->min_speed[$type] = 4;
            if ($i >= count($types))
                $this->speed[$type] = $remaining_speed_for_distribute;
            else
                $this->speed[$type] = mt_rand($this->min_speed[$type], $remaining_speed_for_distribute - ($this->min_speed[$type] * (count($types) - $i)));
            $remaining_speed_for_distribute -= $this->speed[$type];
            $i++;
        }
    }

}
