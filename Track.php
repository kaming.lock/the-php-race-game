<?php
require_once('Track_element.php');

class Track
{
    public $total = 0;
    public $elements_per_series = 0;
    public $elements = array();

    public function __construct() {
        $this->total = 2000;
        $this->elements_per_series = 40;
        $this->elements = array();

        $refl = new ReflectionClass('Track_element');
        $types = array_filter($refl->getConstants(), function($key) { return substr($key, 0, 5) == 'TYPE_'; }, ARRAY_FILTER_USE_KEY);

        for ($i = 0; $i < $this->total; $i++) {
            $element = new Track_element();
            if ($i % $this->elements_per_series == 0) {
                $element->type = array_values($types)[mt_rand(0, count($types) - 1)];
            } else {
                $element->type = $this->elements[$i - 1]->type;
            }
            $this->elements[$i] = $element;
        }
    }

}
