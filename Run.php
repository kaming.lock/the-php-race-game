<?php
include('Race.php');


// check PHP version before execution
verify_php_version();

// run a race and print the results
 $test = new Race;
 $results = $test->runRace();
 print_r($results->getRoundResults());

// you can uncomment this line to show the result in text.
//  echo get_announce_result_html();

function get_announce_result_html() {
    extract($GLOBALS);
    $msgs = array();
    if (!isset($test)) $msgs[] = 'Has the race setup yet?';
    if (!isset($results)) $msgs[] = 'Has the race started yet?';
    if (!empty($msgs)) return '<pre><p>'.implode('<br/>', $msgs).'</p></pre>';
    $table = '<table><thead><tr><td>#</td><td>ID</td></tr></thead><tbody>';
    foreach ($test->get_particpants_info() as $car_idx => $car) {
        $table .= '<tr><td>'.$car_idx.'</td><td>'.$car->id.'</td></tr>';
    }
    $table .= '</tbody></table>';
    $msgs[] = $table;
    $winners = $results->getRoundWinners();
    $outcome = '';
    switch ($results->getRoundOutcome()) {
        default:
        case RaceResult::OUTCOME_PENDING: $msgs[] = 'We are still waiting the race to begin.'; break;
        case RaceResult::OUTCOME_WIN: $msgs[] = sprintf('The car with ID: %s wins this.', array_shift($winners)->id); break;
        case RaceResult::OUTCOME_LOSE: $msgs[] = 'No one wins this one.'; break;
        case RaceResult::OUTCOME_DRAW: $msgs[] = ucfirst(sprintf('%s all togehter arrived the finish line at the same time. It is a draw.', implode(' and ', array_map(function($winner) { return 'the car with ID: '.$winner->id; }, $winners)))); break;
    }
    return '<pre><p>'.implode('<br/>', $msgs).'</p></pre>';
}

function verify_php_version() {
    $msg = '';
    if (version_compare(PHP_VERSION, '7.4.0', '<')) $msg = 'Please update your PHP version to at least 7.4.0';
    elseif (version_compare(PHP_VERSION, '8.1.0', '>=')) $msg = 'Your PHP version is too new! Please downgrade to PHP version 8.0.0 to run this application';
    echo $msg;
    if (!empty($msg)) exit;
}
