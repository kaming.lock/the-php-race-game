<?php

class Track_element
{
    const TYPE_STRAIGHT = 'straight';
    const TYPE_CURVE = 'curve';

    public $type;
    public $length;

    public function __construct() {
        $this->length = 1;
    }

}
