<?php
require_once('Track.php');
require_once('Car.php');
require_once('RaceResult.php');

class Race
{
    public $carsAmount = 0;

    private $cars;

    public function __construct() {
        $this->carsAmount = 5;
    }

    public function runRace(): RaceResult
    {
        $tracks_and_cars = array();
        $track = new Track(); // use the same track for all cars.
        for ($i = 0; $i < $this->carsAmount; $i++) {
            $car = new Car();
            $this->cars[] = $car;
            $tracks_and_cars[] = compact('track', 'car');
        }
        $race_result = new RaceResult($tracks_and_cars);
        return $race_result;
    }

    public function get_particpants_info(): array
    {
        return $this->cars;
    }

}
